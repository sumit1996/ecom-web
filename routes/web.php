<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/', 'HomeController@index')->name('index');
Route::get('post/{id}', 'HomeController@posts')->name('post');

Route::get('product-details', 'HomeController@product_details');
Route::get('cart/{productid}/store', 'ProductController@store');
Route::post('add_to_cart', 'HomeController@add_to_cart');
Route::post('add_to_wishlist', 'HomeController@add_to_wishlist');
Route::get('cart', 'HomeController@cart');


Route::group(['middleware' => ['auth']], function () {
Route::post('/cart-update/', 'ProductController@save_cart')->name('update_cart');
});
