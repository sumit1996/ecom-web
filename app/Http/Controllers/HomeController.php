<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Post;
use App\Product;
use App\Slider;
use App\User;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * view a home page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(){

        $sliders = Slider::all();
        $posts = Post::all();
        $products = Product::all();

        return view('pages.home',['sliders' => $sliders , 'posts' => $posts, 'products' => $products]);
    }

    function posts(){
        return view('pages.post');
    }
    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function product_details(Request $request)
    {
        $products = Product::where("id", $request->productId)->get();
        return view('pages.product-details')->with('products', $products);
    }

    /**
     * save detail in add to cart
     */
    function add_to_cart(Request $request)
    {
        if (Auth::user()) {   // Check is user logged in
            $data = $request->all();
            $user = Auth::user();
            $product_id = $data['product_id'];
            $user_id = $user->id;
            $cart_exist =  Cart::where('UserId', $user_id)->where('ProductId', $product_id)->select('id')->count();
            if($cart_exist != 0){
                return response()->json([
                    'results' => "exist",
                    'success' => true
                ]);
            }else{

                $cart = new Cart();
                $cart->UserId = $user_id;
                $cart->ProductId = $product_id;
                $cart->save();

                return response()->json([
                    'results' => "insert",
                    'success' => true
                ]);
            }

        } else {
            return response()->json([
                'results' => "logout",
                'success' => false
            ]);

        }

    }

    /**
     * save detail in add to wishlist
     */
    function add_to_wishlist(Request $request)
    {
        if (Auth::user()) {   // Check is user logged in
            $data = $request->all();
            $user = Auth::user();
            $product_id = $data['product_id'];
            $user_id = $user->id;
            $cart_exist =  Wishlist::where('UserId', $user_id)->where('ProductId', $product_id)->select('id')->count();
            if($cart_exist != 0){
                return response()->json([
                    'results' => "exist",
                    'success' => true
                ]);
            }else{

                $cart = new Wishlist();
                $cart->UserId = $user_id;
                $cart->ProductId = $product_id;
                $cart->save();

                return response()->json([
                    'results' => "insert",
                    'success' => true
                ]);
            }

        } else {
            return response()->json([
                'results' => "logout",
                'success' => false
            ]);

        }

    }

    /**
     * show cart value in header
     */
    public static function get_number_of_cart($id){
        $cart = Cart::where('UserId', $id)->get()->count();
        return $cart;
    }

    /**
     * view cart page
     */
    function cart(){
        $user = Auth::user();
        $user_id = $user->id;
        $carts = Cart::where('UserId', $user_id)->get();
        $products = array();
        foreach ($carts as $key => $cart){
            $products[$key] = Product::find($cart->ProductId);
        }
        return view('pages.cart')->with('products', $products)->with('user_id', $user_id);
    }

}
