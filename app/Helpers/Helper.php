<?php


namespace App\Helpers;

use App\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;

use DateTime;

class Helper

{

    /**
     * get cart details
     */
    public static function get_cart_details($product_id,$user_id)
    {
        $cart_id = Cart::where('UserId', $user_id)->where('ProductId', $product_id)->select("Quantity")->take(1)->get();
        return $cart_id[0]["Quantity"];
    }

}
