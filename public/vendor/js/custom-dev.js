$(document).ready(function () {
   $('.cart-save').on('click', function () {
       var product_id = $(this).parents('.category').find('#product-id').val();
       var domain = window.location.origin;
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
       $.ajax({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           },
           type: "POST",
           dataType: "json",
           url: domain + "/add_to_cart",
           beforeSend: function () {
               // $(".search-loader").fadeIn('swing');
           },
           error: function (error) {
               // $(".search-loader").fadeOut('swing')
           },
           data: {product_id: product_id},
           success: function (results) {
               var cart = results.results;

               if(cart == "insert"){
                   $("#cart-popup-insert").modal("show");
                   setTimeout(function () {
                       $("#cart-popup-insert").modal("hide")
                   }, 3000)
               }else if(cart == "logout"){
                   $("#cart-popup-logout").modal("show");
                   setTimeout(function () {
                       $("#cart-popup-logout").modal("hide")
                   }, 3000)
               }else if(cart == "exist"){
                   $("#cart-popup-exist").modal("show");
                   setTimeout(function () {
                       $("#cart-popup-exist").modal("hide")
                   }, 3000)
               }

           }
       })
   });


    $('.wishlist-save').on('click', function () {
        var product_id = $(this).parents('.category').find('#product-id').val();
        var domain = window.location.origin;
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            type: "POST",
            dataType: "json",
            url: domain + "/add_to_wishlist",
            beforeSend: function () {
                // $(".search-loader").fadeIn('swing');
            },
            error: function (error) {
                // $(".search-loader").fadeOut('swing')
            },
            data: {product_id: product_id},
            success: function (results) {
                var wishlist = results.results;

                if(wishlist == "insert"){
                    $("#wishlist-popup-insert").modal("show");
                    setTimeout(function () {
                        $("#wishlist-popup-insert").modal("hide")
                    }, 3000)
                }else if(wishlist == "logout"){
                    $("#wishlist-popup-logout").modal("show");
                    setTimeout(function () {
                        $("#wishlist-popup-logout").modal("hide")
                    }, 3000)
                }else if(wishlist == "exist"){
                    $("#wishlist-popup-exist").modal("show");
                    setTimeout(function () {
                        $("#wishlist-popup-exist").modal("hide")
                    }, 3000)
                }

            }
        })
    });

    // remove from cart

    $('.product-remove').on('click', function () {
        // var product_id = $(this).parents('.category').find('#product-id').val();
        alert("product_id");
        // var domain = window.location.origin;
        // $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        // $.ajax({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        //     },
        //     type: "POST",
        //     dataType: "json",
        //     url: domain + "/add_to_wishlist",
        //     beforeSend: function () {
        //         // $(".search-loader").fadeIn('swing');
        //     },
        //     error: function (error) {
        //         // $(".search-loader").fadeOut('swing')
        //     },
        //     data: {product_id: product_id},
        //     success: function (results) {
        //         var wishlist = results.results;
        //
        //         if(wishlist == "insert"){
        //             $("#wishlist-popup-insert").modal("show");
        //             setTimeout(function () {
        //                 $("#wishlist-popup-insert").modal("hide")
        //             }, 3000)
        //         }else if(wishlist == "logout"){
        //             $("#wishlist-popup-logout").modal("show");
        //             setTimeout(function () {
        //                 $("#wishlist-popup-logout").modal("hide")
        //             }, 3000)
        //         }else if(wishlist == "exist"){
        //             $("#wishlist-popup-exist").modal("show");
        //             setTimeout(function () {
        //                 $("#wishlist-popup-exist").modal("hide")
        //             }, 3000)
        //         }
        //
        //     }
        // })
    });



});
